Advanced react features:
- Side Effects
- Reducers
- Context API

Hooks used:
- useState
- useEffect
- useReducer
- useContext
- useRef
- useImperativeHandle
- forwardRef